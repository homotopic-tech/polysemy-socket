# Changelog for polysemy-socket

## v0.0.2.0

* Remove dependency on polysemy-plugin.

## v0.0.1.0

* Added `Socket` effect with some interpreters.
