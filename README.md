# polysemy-socket

Experimental Socket effect for polysemy. The interface is made to mirror the
TCP interface in [socket](https://hackage.haskell.org/package/socket), but
is subject to change.
